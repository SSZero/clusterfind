﻿using System;
using System.IO;

namespace ClusterFind.Example1.FindChainsInInputFile
{
    /// <summary>
    /// This example reads an input file and calculates how many clusters are in the input data.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {

            int[,] grid = readGridFromFile("..\\..\\input1.txt");
            Console.WriteLine();

            ClusterFinder cf = new ClusterFinder();
            var clusters = cf.FindClusters(grid);

            Console.WriteLine("Game board:");
            Print2DArray(grid);
            Console.WriteLine();
            
            Console.WriteLine("Connected groups:");
            Print2DArray(clusters);
            Console.WriteLine();

            Console.WriteLine("Clusters of any size:\n===================");
            var clustersOfSize1 = cf.GetClustersWithSize(1);

            foreach (Cluster c in clustersOfSize3)
            {
                Console.WriteLine(c.ToString());
            }

            Console.ReadKey();
        }

        /// <summary>
        /// Reads an input file consisting of a header with two values for the size of the arrays, and Xs and spaces.
        /// </summary>
        /// <param name="file">The name of the file</param>
        /// <returns></returns>
        public static int[,] readGridFromFile(string file)
        {
            int[,] grid;
            FileInfo f = new FileInfo(file);
            using (var fs = new StreamReader(f.OpenRead()))
            {
                Console.WriteLine("Input: ");
                const char MARKER = 'x';
                var header = fs.ReadLine();
                string[] lengths = header.Split(' ');

                int lenX = int.Parse(lengths[0]);
                int lenY = int.Parse(lengths[1]);

                grid = new int[lenX, lenY];

                int lineNum = 0;
                while (!fs.EndOfStream)
                {

                    var line = fs.ReadLine();
                    Console.WriteLine(line);
                    for (int i = 0; i < line.Length; i++)
                    {
                        grid[lineNum, i] = line[i] == MARKER ? 1 : 0;
                    }

                    lineNum++;
                }
            }
            return grid;
        }
        public static void Print2DArray<T>(T[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
        }
    }
}
