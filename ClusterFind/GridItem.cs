﻿

namespace ClusterFind
{
    public class GridItem
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int ID { get; set; }
        public int Value { get; set; }

        public override string ToString()
        {
            return string.Format("Item X: {0} Y: {1} Value: {2} Cluster ID: {3}", X, Y, Value, ID);
        }
    }
}
