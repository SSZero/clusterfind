﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClusterFind
{
    public class Cluster
    {
        private List<GridItem> items;
        private int id;

        public Cluster(int id)
        {
            items = new List<GridItem>();
            this.id = id;
        }

        public void Add(GridItem c)
        {
            items.Add(c);
        }

        public int ID()
        {
            return id;
        }

        public int Size()
        {
            return items.Count;
        }

        public List<GridItem> GetCoordsInCluster()
        {
            // return a copy of the original list
            var newList = new List<GridItem>(items);
            return newList;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Cluster "+id+"\n");
            sb.Append("Size: " + Size() + "\n");
            foreach (GridItem g in items)
            {
                sb.Append(g+"\n");
            }
            return sb.ToString();
        }
    }
}
