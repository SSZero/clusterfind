﻿using System;
using System.IO;

namespace ClusterFind
{
    class Program
    {
        static void Main(string[] args)
        {

            int[,] grid = new int[5, 5]
            {
                { 2, 0, 1, 1, 5},
                { 2, 2, 1, 1, 5},
                { 1, 0, 0, 3, 1},
                { 1, 1, 1, 4, 2},
                { 1, 0, 4, 4, 2},
            };

            Console.WriteLine();
            ClusterFinder cf = new ClusterFinder();
            var clusters = cf.FindClusters(grid);
            Console.WriteLine("Game board:");
            Print2DArray(grid);
            Console.WriteLine();
            //Console.WriteLine(clusters);
            Console.WriteLine("Connected groups:");
            Print2DArray(clusters);
            Console.WriteLine();
            Console.WriteLine("Clusters of size 3 or more:\n===================");
            var clustersOfSize3 = cf.GetClustersWithSize(3);

            foreach(Cluster c in clustersOfSize3)
            {
                Console.WriteLine(c.ToString());
            }

            Console.ReadKey();
        }

        public static void Print2DArray<T>(T[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
        }
    }
}
