﻿using System.Collections.Generic;

namespace ClusterFind
{
    public class ClusterFinder
    {
        int[,] connectedComponents;
        bool[,] marked;

        private Dictionary<int, Cluster> clusters;

        int count;

        public ClusterFinder()
        {
            clusters = new Dictionary<int, Cluster>();
        }

        public int[,] FindClusters(int [,] grid)
        {

            int lenY = grid.GetLength(1);
            int lenX = grid.GetLength(0);

            connectedComponents = new int[lenX, lenY];
            marked = new bool[lenX, lenY];
            count = 1;
            for (int i = 0; i < lenX; i++)
            {
                for (int j = 0; j < lenY; j++)
                {
                    if (!marked[i, j] && grid[i,j] != 0) {
                        dfs(grid, i, j, grid[i,j]);
                        count++;
                    }
                }
            }

            return connectedComponents;
        }

        public List<Cluster> GetClustersWithSize(int size)
        {
            List<Cluster> matchingClusters = new List<Cluster>();

            foreach(int clusterId in clusters.Keys)
            {
                var cluster = clusters[clusterId];
                if (cluster.Size() >= size)
                {
                    matchingClusters.Add(cluster);
                }
            }
            return matchingClusters;
        }

        private void dfs(int[,] grid, int x, int y, int value)
        {
            // does not match value, therefor not part of this cluster
            if (grid[x, y] != value)
            {
                return;
            }

            int lenY = grid.GetLength(1);
            int lenX = grid.GetLength(0);

            marked[x, y] = true;
            
            connectedComponents[x, y] = count;

            GridItem item = new GridItem
            {
                X = x,
                Y = y,
                ID = connectedComponents[x, y],
                Value = grid[x, y]
            };

            if (!clusters.ContainsKey(item.ID))
            {
                clusters.Add(item.ID, new Cluster(item.ID));
            }

            clusters[item.ID].Add(item);

            List<GridItem> adjacent = new List<GridItem>();

            if (x > 0)
            {
                adjacent.Add(new GridItem { X = x - 1, Y = y });
            }

            if (x < lenX - 1)
            {
                adjacent.Add(new GridItem { X = x + 1, Y = y });
            }

            if(y > 0)
            {
                adjacent.Add(new GridItem { X = x, Y = y - 1 });
            }

            if (y < lenY - 1)
            {
                adjacent.Add(new GridItem { X = x, Y = y + 1 });
            }
            
            foreach(GridItem coord in adjacent)
            {
                if(!marked[coord.X, coord.Y])
                {
                    dfs(grid, coord.X, coord.Y, value);
                }
            }
        }
    }
}
