﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClusterFind
{
    class Stack<T>
    {
        List<T> items;

        public Stack(){
            items = new List<T>();
        }

        public Stack(int initialCapacity)
        {
            items = new List<T>(initialCapacity);
        }

        public void Push(T item)
        {
            items.Add(item);
        }

        public T Pop()
        {
            var top = items.Last();
            items.Remove(top);
            return top;
        }

        public T Peek()
        {
            var top = items.Last();
            return top;
        }
    }
}
